<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>PRIORITY - Air Minum</title>
      <meta name="description" content="CV PRIORITY INTI RAYA didirikan di Semarang Tgl 30 Jully 2011. Priority di dirikan karena adannya  kepedulian perusahaan kami akan kesehatan masyarakat di Indonesia. Kepedulian kami terhadap kesehatan masyarakat salah satunya adalah kebiasaan sehari hari kita untuk mengkonsumsi air minum yang berkualitas untuk menjaga kesehatan kita"/>
      <meta name="keywords" content="priority,air minum,PRIORITY INTI RAYA"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/priority.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css' />
      <link rel="stylesheet" type="text/css" href="style/960.css" />
      <link rel="stylesheet" type="text/css" href="style/style.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <script type="text/javascript" src="js/sticky/stickyfloat.js"></script>    
    </head>
    
    <body>
      <img id="main-bg" src="images/bg-body.jpg">
      <div class="wrapper">
        
        <div class="container_12">

          <?php include"header.php";?>
          <div class="grid_12"><div class="separator"></div></div>
          <div class="clear"></div>
          <!-- content begin -->
          
          <div id="content">
            <div class="grid_12">
              <div class="page-wrapper about">
                <div class="grid_4 omega sticky">
                  <div class="left-about margin-top wrapper-left">
                    <div class="left-menu">
                      <h3>About Priority</h3>
                      <ul>
                        <li><a href="#" id="go-sejarah">Sejarah</a></li>
                        <li><a href="#" id="go-visi">Visi & Misi</a></li>
                        <li><a href="#" id="go-proses">Pemrosesan</a></li>
                      </ul>
                    </div>
                    <div class="promo">
                      <img src="images/promo.jpg" alt="promo">
                    </div>
                  </div>
                </div>
                <div class="grid_8 omega right">
                  <div class="right-about">
  
                    <div id="sejarah">
                      <h4>Sejarah</h4>
                      <p>CV PRIORITY INTI RAYA didirikan di Semarang Tanggal 30 Juli 2011. Priority didirikan karena adanya  kepedulian perusahaan kami akan kesehatan masyarakat di Indonesia. Kepedulian kami terhadap kesehatan masyarakat salah satunya adalah kebiasaan sehari hari kita untuk mengkonsumsi air minum yang berkualitas untuk menjaga kesehatan kita. </p>
                      <div class="img-box right img-leftmargin">
                        <img class="margin-left" src="images/thumb1.jpg" alt="sejarah"/>

                      </div>
                      <p>Berangkat dari kepedulian itu, maka diciptakanlah produk Air Minum Priority yang murah dan juga berguna untuk menjaga      kesehatan kita. Produk kami diawasi oleh ahli mikrobiologi serta QC (<span class="italic">Quality Control</span>) yang ketat dan juga sudah memenuhi standar             SNI 01-3553-2006 karena komitmen kami untuk menciptakan air minum yang berfungsi untuk menyehatkan masyarakat Indonesia.</p>
                      <div class="clear"></div>
                    </div>
                    <div id="visi">
                      <h4>Visi & Misi</h4>
                      <p>Dalam menjalankan komitmen kami, kami memiliki dasar pondasi perusahaan kami sebagai berikut :</p>
                      <p>Dasar</p>
                      <div class="quote">
                        <span class="italic green">“The Healthy Drinking Water For Health Life.”</span>
                      </div>
                      
                      <p>Visi</p>
                      <div class="quote">
                        <span class="green">Menciptakan kehidupan sehat bagi manusia.</span>
                      </div>
                      
                      <p>Misi</p>
                      <div class="quote">
                        <span class="green">Menyehatkan umat manusia untuk kualitas hidup dan lingkungan hidup lebih sehat dan baik</span>
                      </div>
                      
                    </div>
                    <div id="proses">
                      <h4 id="proses">Pemrosesan</h4>
                      <p>Kelebihan dari air minum yang kita produksi adalah air minum kami bebas dari kandungan-kandungan yang tidak berguna di dalam tubuh sehingga air minum kita lebih mudah di serap oleh tubuh. Keunggulan lainnya adalah air minum kita di lengkapi oleh kadar Oksigen yang tinggi dan juga menggunakan sistem UV <span class="italic">Ultra Violet</span> kesterilan dan kemurnian air kita.</p>
                      <div class="img-box left img-rightmargin">
                        <img class="margin-left" src="images/thumb2.jpg" alt="sejarah"/>
                      </div>
                      <p>Dengan mengkonsumsi Air Minum Priority maka tubuh kita akan menjadi lebih sehat, berbagai penyakit sembuh dan juga lebih awet muda. Mari kita tingkatkan kesehatan dan juga kualitas hidup kita untuk menjadi lebih baik bersama Air Minum Priority.</p>
                      <div class="clear"></div>
                    </div>
                    
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <?php include"footer.php"; ?>
        </div>
      </div>

      <script type="text/javascript">
        $(document).ready(function(){
          // var top = $('.left-about').offset().top - parseFloat($('.left-about').css('marginTop').replace(/auto/, 0));
          // $(window).scroll(function (event) {
          //   // what the y position of the scroll is
          //   var y = $(this).scrollTop();
          //   // whether that's below the form
          //   if (y >= top) {
          //     // if so, ad the fixed class
          //     $('.left-about').addClass('fixed');
          //   } else {
          //     // otherwise remove it
          //     $('.left-about').removeClass('fixed');
          //   }
          // });
          jQuery('.sticky').stickyfloat( {duration: 400} );
          var sejarahtop = jQuery('#sejarah').offset().top;
          var visitop = jQuery('#visi').offset().top;
          var prosestop = jQuery('#proses').offset().top;
          // about klik
          $('#go-sejarah').click(function(){
            $('html, body').animate({scrollTop:sejarahtop}, 'slow');
            return false;
          });
          // visi klik
          $('#go-visi').click(function(){
            $('html, body').animate({scrollTop:visitop}, 'slow');
            return false;
          });
          // misi klik
          $('#go-proses').click(function(){
            $('html, body').animate({scrollTop:prosestop}, 'slow');
            return false;
          });
        });
        
      </script>
    </body>
</html>
