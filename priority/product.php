<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>PRIORITY - Air Minum</title>
      <meta name="description" content="CV PRIORITY INTI RAYA didirikan di Semarang Tgl 30 Jully 2011. Priority di dirikan karena adannya  kepedulian perusahaan kami akan kesehatan masyarakat di Indonesia. Kepedulian kami terhadap kesehatan masyarakat salah satunya adalah kebiasaan sehari hari kita untuk mengkonsumsi air minum yang berkualitas untuk menjaga kesehatan kita"/>
      <meta name="keywords" content="priority,air minum,PRIORITY INTI RAYA"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/priority.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css' />
      <link rel="stylesheet" type="text/css" href="style/960.css" />
      <link rel="stylesheet" type="text/css" href="style/style.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <script type="text/javascript" src="js/sticky/stickyfloat.js"></script>    
    </head>
    
    <body>
      <img id="main-bg" src="images/bg-body.jpg" alt=""/>
      <div class="wrapper">
        
        <div class="container_12">

          <?php include"header.php";?>
          <div class="grid_12"><div class="separator"></div></div>
          <div class="clear"></div>
          <!-- content begin -->
          
          <div id="content">
            <div class="grid_12">
              <div class="page-wrapper product">
                <div class="grid_4 omega sticky">
                  <div class="left-about margin-top wrapper-left">
                    <div class="left-menu">
                      <h3>Category</h3>
                      <ul>
                        <li><a href="#" id="go-air">Air</a></li>
                        <li><a href="#" id="go-why">Air Minum An Organik</a></li>
                        <li><a href="#" id="go-oxygen">Oksigen</a></li>
                        <li><a href="#" id="go-elektro">Elektrolizer</a></li>
                      </ul>
                    </div>
                    
                  </div>
                </div>
                <div class="grid_8 omega right product-right">
                  <div class="right-about">
                    <h4>Product Knowledge</h4>
                    <h3 id="air">Air</h3>
                    
                     <div class="right">
                        <img class="margin-left" src="images/thumb3.jpg" alt="water"/>
                    </div>
                    <p>Air sangat berfungsi untuk kehidupan di muka bumi.Tubuh manusia sangat membutuhkan air. Tubuh kita terdiri atas 70%nya air.Kandungan air yang terdapat di dalam tubuh kita adalah :</p> 
                    <ul>
                      <li>Otak  : 90%</li>
                      <li>Jantung   : 75%</li>
                      <li>Ginjal  : 83%</li>
                      <li>Darah   : 90%</li>
                      <li>Otot  : 75%</li>
                    </ul>
                    
                    <p>Maka dari itu tubuh kita sangat membutuhkan air yang berkualitas untuk diserap tubuh kita salah satunnya dengan mengkonsumsi air minum dengan kadar An Organik rendah. </p><br>
                    <h3 id="why">Mengapa Kita Harus Mengkonsumsi air Minum dengan Kadar An Organik Rendah?</h3>
                    <p>Kita harus mengkonsumsi air yang berkadar An Organik rendah karena kandungan air yang banyak mengandung mineral An Organik dalam bentuk larutan adalah penyebab utama berbagai penyakit. Mineral yang keras lolos dari dinding <span class="italic">Intestinal</span> dan masuk pada <span class="italic">lymphatic system</span>  dan beredar ke seluruh tubuh melalu darah, maka dari itu inilah yang menyebabkan bibit penyakit. Tubuh manusia tidak membutuhkan mineral An Organik dari air karena mineral yang dapat di absorpsi tubuh manusia adalah yang berasal dari makanan seperti sayur maupun buah.</p><br>
                    <h3 id="oxygen">Oksigen</h3>
                    <p>Oksigen (O2) adalah kunci dari segala kehidupan manusia . Manusia tidak dapat hidup tanpa Oksigen. Tubuh dan juga sel-sel di dalam tubuh manusia sangat membutuhkan oksigen. Dengan adannya oksigen, tubuh mampu meregenerasi sel, membantu memperbesar daya serap vitamin dan nutrisi, meningkatkan kekebalan tubuh dan juga menetralkan zat-zat beracun di dalam darah.</p>
                    <p><strong>Manfaat-manfaat Oksigen :</strong></p>
                    
                    <div class="left" style="margin-right:20px;">
                        <img src="images/thumb4.jpg" alt="sejarah"/>
                    </div>
                    
                    <ul class="first">
                      <li>Menstabilkan tekanan darah .</li>
                      <li>Mengurangi stress.</li>
                      <li>Mempercantik kulit dan mencegah penuaan dini.</li>
                      <li>Mampu meningkatkan kecerdasan otak.</li>
                      <li>Mencegah kanker.</li>
                      <li>Menguatkan jantung.</li>
                      <li>Meregenerasi sel-sel yang telah mati.</li>
                      <li>Mengurangi racun tubuh.</li>
                      <li>Menguatkan sistem kekebalan tubuh.</li>
                    </ul>
                    <p>Dengan kandungan mineral An Organik yang rendah dan juga kadar Oksigen yang tinggi sesuai standar SNI 01-3553-2006 Air Minum Priority dapat menyembuhkan berbagai penyakit beberapa manfaat dari Air Minum Priority yang sudah terbukti oleh konsumen kami adalah :</p>
                    <div class="mineral">
                      <ul class="second">
                        <li>Menghilangkan Sakit kepala /Vertigo</li>
                        <li>Menyembuhkan Diare</li>
                        <li>Menyembuhkan Kencing manis</li>
                        <li>Menghilangkan Batu Ginjal</li>
                        <li>Menurunkan kadar Asam urat</li>
                        <li>Menurunkan Darah Tinggi</li>
                        <li>Menyembuhkan penyakit saluran kencing</li>
                        <li>Menyembuhkan radang</li>
                        <li>Menyembuhkan Sariawan</li>
                        <li>Menghilangkan racun dalam tubuh</li>
                        <li>Menghilangkan Kolestrol</li>
                        <li>Meningkatkan stamina tubuh</li>
                        <li>Mempercantik kulit</li>
                      </ul>
                    </div>
                    
                    <h3 id="elektro">Apa itu Elektrolizer</h3>
                    <p>Salah satu alat uji coba yang kita sering uji cobakan adalah Elektrolizer. Elektrolizer adalah alat yang sangat praktis untuk mengetahui kadar air dengan mengadopsi teknologi laboratorium.</p>
                    
                    <p>Dengan alat ini kita bisa melakukan penelitian terhadap kualitas air secara mandiri. Elektrolizer yang beredar di Indonesia kebanyakan dibuat berdasarkan panduan dari <span class="italic">American Environment Protection Bureau</span> (Biro Keselamatan Lingkungan Negara Amerika).</p>
                    <p>Daya kerja elektrolizer mampu menguraikan atau melepaskan ikatan-ikatan zat padat terlarut dalam air melalui sistem anoda katoda. Alat ini cocok digunakan untuk mengetahui tingkat kekeruhan dalam air atau <span class="italic">Total Dissolved Solid</span> (TDS), serta membandingkan dua jenis air yaitu Air Minum Priority dan air biasa kita minum sehari hari.</p>
                    <p>Cara kerjanya juga tak merepotkan, masukkan masing-masing sepasang kaki elektrolizer kedalam masing-masing gelas (bisa dua gelas dengan ukuran sama warna transparan) yang telah diisi air dan hendak dites dengan Air Minum Priority, nyalakan saklar sekitar satu atau dua menit, kemudian hasilnya akan langsung dilihat secara kasat mata.</p>
                    
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <?php include"footer.php"; ?>
        </div>
      </div>

      <script type="text/javascript">
        $(document).ready(function(){
          
          jQuery('.sticky').stickyfloat( {duration: 400} );
          var airtop = jQuery('#air').offset().top;
          var whytop = jQuery('#why').offset().top;
          var oxytop = jQuery('#oxygen').offset().top;
          var elektrotop = jQuery('#elektro').offset().top;
          
          $('#go-air').click(function(){
            $('html, body').animate({scrollTop:airtop}, 'slow');
            return false;
          });
          $('#go-why').click(function(){
            $('html, body').animate({scrollTop:whytop}, 'slow');
            return false;
          });
          $('#go-oxygen').click(function(){
            $('html, body').animate({scrollTop:oxytop}, 'slow');
            return false;
          });
          $('#go-elektro').click(function(){
            $('html, body').animate({scrollTop:elektrotop}, 'slow');
            return false;
          });
        });
        
      </script>
    </body>
</html>
