<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>PRIORITY - Air Minum</title>
      <meta name="description" content="CV PRIORITY INTI RAYA didirikan di Semarang Tgl 30 Jully 2011. Priority di dirikan karena adannya  kepedulian perusahaan kami akan kesehatan masyarakat di Indonesia. Kepedulian kami terhadap kesehatan masyarakat salah satunya adalah kebiasaan sehari hari kita untuk mengkonsumsi air minum yang berkualitas untuk menjaga kesehatan kita"/>
      <meta name="keywords" content="priority,air minum,PRIORITY INTI RAYA"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/priority.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css' />
      <link rel="stylesheet" type="text/css" href="style/960.css" />
      <link rel="stylesheet" type="text/css" href="style/style.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <link rel="stylesheet" href="style/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
      <script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js"></script>
      <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

      <!-- optional -->
      <link rel="stylesheet" href="style/fancybox/jquery.fancybox-buttons.css" type="text/css" media="screen" />
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-buttons.js"></script>
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-media.js"></script>

      <link rel="stylesheet" href="style/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-thumbs.js"></script>     
    </head>
    
    <body>
      <img id="main-bg" src="images/bg-body.jpg" alt="">
      <div class="wrapper">
        <div class="container_12">
          <?php include"header.php";?>
          <div class="grid_12"><div class="separator"></div></div>
          <div class="clear"></div>
          <!-- content begin -->
          
          <div id="content">
            <div class="grid_12">
              <div class="page-wrapper gallery">
                <div class="gallery-wrapper">
                  <h3>Gallery</h3>
                  <div class="pictures">
                    <ul>
                      <li>
                        <a class="picture-box" href="images/Armada-Truk.jpg" rel="fancybox-button" title="Armada truck">
                          <div class="picture">
                            <img src="images/Armada-Truk-thumb.jpg" alt="">
                          </div>
                        </a>
                        <div class="title"><p>Armada Truck</p></div>
                      </li>
                      <li>
                        <a class="picture-box" href="images/Chemical-Laboratorium.jpg" rel="fancybox-button" title="ChemicalLaboratorium">
                          <div class="picture">
                            <img src="images/Chemical-Laboratorium-thumb.jpg" alt=""/>
                          </div>
                        </a>
                        <div class="title"><p>Chemical Laboratorium</p></div>
                      </li>
                      <li>
                        <a class="picture-box" href="images/Filtering-Water-Process.jpg" rel="fancybox-button" title="Filtering Water Process">
                          <div class="picture">
                            <img src="images/Filtering-Water-Process-thumb.jpg" alt=""/>
                          </div>
                        </a>
                        <div class="title"><p>Filling Water Process</p></div>
                      </li>
                      <li class="last-pict">
                        <a class="picture-box" href="images/Packaging-Water-Process.jpg" rel="fancybox-button" title="Packaging Water Process">
                          <div class="picture">
                            <img src="images/Packaging-Water-Process-thumb.jpg" alt=""/>
                          </div>
                        </a>
                        <div class="title"><p>Packaging Water Process</p></div>
                      </li>
                      <div class="clear"></div>
                      <li>
                        <a class="picture-box" href="images/Car-Agent.jpg" rel="fancybox-button" title="Car Agent">
                          <div class="picture">
                            <img src="images/Car-Agent-thumb.jpg" alt="">
                          </div>
                        </a>
                        <div class="title"><p>Car Agent</p></div>
                      </li>
                      <li>
                        <a class="picture-box" href="images/Gallon-Storage.jpg" rel="fancybox-button" title="Gallon Storage">
                          <div class="picture">
                            <img src="images/Gallon-Storage-thumb.jpg" alt=""/>
                          </div>
                        </a>
                        <div class="title"><p>Gallon Storage</p></div>
                      </li>
                      <li>
                        <a class="picture-box" href="images/water-source.jpg" rel="fancybox-button" title="Water Source">
                          <div class="picture">
                            <img src="images/water-source-thumb.jpg" alt=""/>
                          </div>
                        </a>
                        <div class="title"><p>Water Source</p></div>
                      </li>
                      <li class="last-pict">
                        <a class="picture-box" href="images/Stock-Storage.jpg" rel="fancybox-button" title="Stock Storage">
                          <div class="picture">
                            <img src="images/Stock-Storage-thumb.jpg" alt=""/>
                          </div>
                        </a>
                        <div class="title"><p>Stock Storage</p></div>
                      </li>
                      <div class="clear"></div>
                    </ul>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
          <?php include"footer.php"; ?>
        </div>
      </div>
      <script type="text/javascript">
        $(document).ready(function() {
          $(".picture-box").fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            closeBtn    : false,
            helpers   : {
              title : { type : 'inside' },
              buttons : {}
            }
          });
        });
        
      </script>
    </body>
</html>
