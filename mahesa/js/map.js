var id = "map";
var markerLatLng = new google.maps.LatLng(-7.00049,110.425032);
var myLatLng = new google.maps.LatLng(-7.00049,110.41514);
var myZoom = 15;
var image = 'images/location.png'; 
var markerTitle = "Mahesa Komputer";


function initializeMap(){
	var mapOptions = {
      center: myLatLng,
      zoom: myZoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById(id),
        mapOptions);

    var marker = new google.maps.Marker({
      position: markerLatLng,
      map: map,
      title: markerTitle,
      icon: image
	});
}

  



