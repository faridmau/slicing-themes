var id = "map";
var myLatLng = new google.maps.LatLng(-6.988398,110.460987);
var myZoom = 15;
var image = 'images/yellow-dot.png'; /* http://mapicons.nicolasmollet.com/ */
var markerTitle = "Bali Tour";


function initializeMap(){
	var mapOptions = {
      center: myLatLng,
      zoom: myZoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById(id),
        mapOptions);

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: markerTitle,
      icon: image
	});
}

  



