<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <?php include"linkrel.php"; ?>
      <link rel="stylesheet" type="text/css" href="style/nivo-slider/nivo-slider.css" />
      <link rel="stylesheet" type="text/css" href="style/nivo-slider/default.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <script type="text/javascript" src="js/nivo-slider/jquery.nivo.slider.js"></script>
      <script type="text/javascript" src="js/nivo-slider/jquery.nivo.slider.pack.js"></script>
      
    </head>
    
    <body>
      <?php include"header.php"; ?>
      <div class="wrapper">
        <div class="slider theme-default">
          <div class="nivo-slider">
            <img src="images/slide1.jpg" alt="Floris"/>
            <img src="images/slide2.jpg" alt="Floris"/>
          </div>
        </div>
      </div>
      <div class="orange-line"></div>
      <div id="content" class="wrapper">
        <img src="images/flower1.png" class="flower1">
        <div class="main container_12">
          <div class="box">
            <div class="grid_3">
              <div class="wrapper-title">
                <div class="title">
                  <h3 class="left">SYMPATHY</h3>
                  <h5 class="left">flowers</h5>
                  <div class="clear"></div>
                </div>
              </div>
              <img src="images/thumb2.jpg" alt="flower" class="radius">
              <h4>Sweetmemory</h4>
            </div>
            <div class="grid_3">
              <div class="wrapper-title">
                <div class="title">
                  <h3 class="left">WEDDING</h3>
                  <h5 class="left">flowers</h5>
                  <div class="clear"></div>
                </div>
              </div>
              <img src="images/thumb1.jpg" alt="flower" class="radius">
              <h4>handbouquet</h4>
            </div>
            <div class="grid_3">
              <div class="wrapper-title">
                <div class="title">
                  <h3 class="left">DECORATION</h3>
                  <h5 class="left">flowers</h5>
                  <div class="clear"></div>
                </div>
              </div>
              <img src="images/thumb3.jpg" alt="flower" class="radius">
              <h4>Ovaly Red</h4>
            </div>
            <div class="grid_3">
              <div class="wrapper-title">
                <div class="title">
                  <h3 class="left">BIRTHDAY</h3>
                  <h5 class="left">flowers</h5>
                  <div class="clear"></div>
                </div>
              </div>
              <img src="images/thumb4.jpg" alt="flower" class="radius">
              <h4>Warm Shine</h4>
            </div>
          <div class="clear"></div>
          </div>
          <div class="welcome">
            <div class="grid_8">
              <h3>WELCOME</h3>
              <p><span class="orange">Vivi Florist</span> adalah Toko Bunga yang menjual aneka rangkaian dan karangan bunga indah dan segar, terdiri dari aneka Bunga Lokal dan Bunga Impor Terbaik dan Murah. Juga menyediakan bunga artificial yang bisa menghias ruangan anda.</p>
              <p>Kami menyediakan jasa layan antar bunga ke semua tempat tujuan anda dimanapun yang anda inginkan.</p>
              <p>Kami akan dengan senang hati membantu anda dalam menentukan pilihan bunga dari berbagai kebutuhan anda, baik untuk koleksi pribadi atau dikirim ke rekanan anda.</p>
              <a href="#" class="maron right italic">read more...</a>
            </div>
            <div class="grid_4 call-wrapp">
              <div class="call">
                <p>Hubungi Kami</p>
                <p>Sekarang Untuk</p>
                <p>Penawaran Terbaik</p>
                <p class="phone">024 702 27 445</p>
                <p>atau</p>
                <div class="klik">
                  <a href="#" class="button radius">klik disini</a>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <?php include"footer.php"; ?>
      <script type="text/javascript">
        $(document).ready(function(){
          $('.nivo-slider').nivoSlider();

        });
      </script>
    </body>
</html>