<?php
  /**
   * Script untuk Send Email
   */
  $strError = "";
  //email pengirim
  $email_submit = "venario@rectmedia.com";

  //BILA PAKAI RECAPTCHA
 /* require_once('./js/recaptchalib.php');
  //$options = get_option( 'recaptcha_options' );
  $publickey = '6LdRf8kSAAAAAOWawp99hLoQLPnOBkktantW9fJ3';
  $privatekey = '6LdRf8kSAAAAAGsfoMgupEcsnZYfezDDZrqWjUW_';
  
 
  //the response from reCAPTCHA
  $resp = null;
  # the error code from reCAPTCHA, if any
  $error = null;
  //SAMPAI DI SINI RECAPTCHA*/
  if (isset($_POST['action']) == "send") {
  $name = $_POST["name"];
  $email = $_POST["email"];
  $telepon = $_POST["phone"];
  $subject = $_POST["subject"];
  $message = $_POST["pesan"];  
  $strMessage = '
          Hi,
          <br />
          Seseorang mengirim email melalui Form Contact dari website www.kolamku.com, berikut detailnya :
          <br />
          Data Diri :
          <table width="400px" style="margin-left:40px">
          <tr><td width="30%">Nama :</td><td>'.$name.'</td></tr>
          <tr><td width="30%">Email :</td><td>'.$email.'</td></tr>
          <tr><td width="30%">Telepon :</td><td>'.$telepon.'</td></tr>
          </table>
          <br />
          Pesan yang disampaikan :
          <table width="400px" style="margin-left:40px">
          <tr>
            <td width="30%">Judul :</td><td>'.$subject.'</td>
          </tr>
          <tr>
            <td width="30%">Pesan :</td><td>'.$message.'</td>
          </tr>
          </table>
      ';

    $strTo = $email_submit;
    $strSubject = '[PARADISE] Anda mendapatkan pesan dari '.$name.' melalui portal pesan www.kolamku.com';
    $strMessage = nl2br($strMessage);

    $strSid = md5(uniqid(time()));

    $strHeader = "";
    $strHeader .= "From: ".$name."<".$email.">\nReply-To: ".$email."\n";

    $strHeader .= "MIME-Version: 1.0\n";
    $strHeader .= "Content-Type: multipart/mixed; boundary=\"".$strSid."\"\n\n";
    $strHeader .= "This is a multi-part message in MIME format.\n";

    $strHeader .= "--".$strSid."\n";
    $strHeader .= "Content-type: text/html; charset=utf-8\n";
    $strHeader .= "Content-Transfer-Encoding: 7bit\n\n";
    $strHeader .= $strMessage."\n\n";

          # was there a reCAPTCHA response?
          # JIKA PAKAI RECAPTCHA
      /*  if ($_POST["recaptcha_response_field"]) {
                  $resp = recaptcha_check_answer ($privatekey,
                                                  $_SERVER["REMOTE_ADDR"],
                                                  $_POST["recaptcha_challenge_field"],
                                                  $_POST["recaptcha_response_field"]);

                  if ($resp->is_valid) {*/
                      $flgSend = @mail($strTo,$strSubject,null,$strHeader);  // @ = No Show Error //

                      if($flgSend)
                      {
                              $strError = '<h3 id="successmessage">Terima kasih, pesan berhasil terkirim</h3>';
                      }
                      else
                      {
                              $strError = '<h3 id="errormessage">Maaf, pesan gagal terkirim</h3>';
                      }
          // JIKA PAKAI RECAPTCHA              
          /*                          
                  } else {
                          # set the error code so that we can display it
                          $error = $resp->error;
                          $strError = "<br><center><h2>".$error."</h2></center><br>";
                  }
          }*/

  }
  
  ?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <?php include"linkrel.php"; ?>
      <script type="text/javascript" src="js/jquery.validationEngine-en.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHAd5EtGvzkpyMJ3C_qNEb81ujYCB7rEA&sensor=false"></script>
      <script type="text/javascript" src="js/map.js"></script>
      <link rel="stylesheet" type="text/css" href="style/validationEngine.jquery.css">
    </head>
    <body>
      <?php include"header.php"; ?>
      <div class="orange-line"></div>
      <div id="content" class="wrapper">
        <img src="images/flower1.png" class="flower1">
        <div id="product" class="main container_12">
          <div class="grid_8 suffix_4">
            <h2>CONTACT US</h2>
          </div>
          <div class="clear"></div>
          <div class="grid_8 left-contact">
            <div id="map"></div>
            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sed odio dui.</p>
            <form id="contact_form" name="contact_form" action="contact.php" method="post">
              <input type="hidden" name="action" value="send" />
              <div class="line">
                <label>Nama</label>
                <p>:</p>
                <input type="text" name="name" class="validate[required]"/>
                <div class="clear"></div>
              </div>
              <div class="line">
                <label>Email</label>
                <p>:</p>
                <input type="text"name="email" class="validate[required,custom[email]]"/>
                <div class="clear"></div>
              </div>
              <div class="line">
                <label>Telepone</label>
                <p>:</p>
                <input type="text"name="phone" class="validate[required,custom[integer],maxSize[20]]"/>
                <div class="clear"></div>
              </div>
              <div class="line">
                <label>Subjek</label>
                <p>:</p>
                <input type="text"name="subject" class="validate[required]"/>
                <div class="clear"></div>
              </div>
              <div class="line">
                <label>Pesan</label>
                <p>:</p>
                <textarea name="pesan" class="validate[required]"></textarea>
                <div class="clear"></div>
              </div>
              <div class="submit right"><input type="submit" class="button radius bg-maron"></div>
              <div class="clear"></div>
            </form>
          </div>
          <div class="grid_4 right-contact">
            <img src="images/contact.jpg" class="radius">
            <div class="location">
              <p class="blue">Kantor Pusat</p>
              <p>Grand Marina Blok 8 No 12, Semarang</p>
              <p>Jawa Tengah</p>
            </div>
            <div class="telepon">
              <p class="left type">Phone</p>
              <p class="left">:</p>
              <p class="left num"><span>024 70157400</span></p>
              <div class="clear"></div>
            </div>
            <div class="telepon">
              <p class="left type">Fax</p>
              <p class="left">:</p>
              <p class="left num"><span>024 7608404</span></p>
              <div class="clear"></div>
            </div>
            <div class="telepon">
              <p class="left type">Email</p>
              <p class="left">:</p>
              <p class="left num"><span>noviana_the@yahoo.com tunindo.abadi2yahoo.com</span></p>
              <div class="clear"></div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <?php include"footer.php"; ?>
      <script type="text/javascript">
        $(document).ready(function(){
          initializeMap();
          $("#contact_form").validationEngine();
        });
      </script>
    </body>
</html>