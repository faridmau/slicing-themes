var id = "map";
var markerLatLng = new google.maps.LatLng(-6.95424,110.395973);
var myLatLng = new google.maps.LatLng(-6.95424,110.395973);
var myZoom = 15;
var image = 'images/location.png'; 
var markerTitle = "VIVI FLORIS";


function initializeMap(){
	var mapOptions = {
      center: myLatLng,
      zoom: myZoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById(id),
        mapOptions);

    var marker = new google.maps.Marker({
      position: markerLatLng,
      map: map,
      title: markerTitle,
      icon: image
	});
}

  



