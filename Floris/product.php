<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <?php include"linkrel.php"; ?>
      <script type="text/javascript" src="js/sticky/stickyfloat.js"></script>
    </head>
    <body>
      <?php include"header.php"; ?>
      
      <div class="orange-line"></div>
      <div id="content" class="wrapper">
        <img src="images/flower1.png" class="flower1">
        <div id="product" class="main container_12">
          <div class="grid_9 prefix_3">
            <h2>PRODUCT</h2>
          </div>
          <div class="clear"></div>
          <div class="grid_3 menu">
            <div class="left-menu">
              <h4>CATEGORY</h4>
              <ul>
                <li><a href="#" id="gofresh">Fresh Flower</a></li>
                <li><a href="#" id="goart">Artificial</a></li>
                <li><a href="#" id="gostick">Stick Werk</a></li>
                <li><a href="#" id="gosou">Souvenir</a></li>
              </ul>
            </div>
            
          </div>
          <div class="grid_9 right">
            <div class="right-product">
              <div id="fresh">
                <h4>Fresh Flower</h4>
                <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Sed posuere consectetur est at lobortis. Maecenas faucibus mollis interdum.Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Sed posuere consectetur est at lobortis. Maecenas faucibus mollis interdum.</p>
                <div class="gallery-wrapper">
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/fresh1.jpg" alt="fresh flowers" class="radius">
                      <h5>Fresh 01</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/fresh2.jpg" alt="fresh flowers" class="radius">
                      <h5>Fresh 01</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/fresh3.jpg" alt="fresh flowers" class="radius">
                      <h5>Fresh 01</h5>
                    </div>
                  </div>
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/fresh4.jpg" alt="fresh flowers" class="radius">
                      <h5>Fresh 01</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/fresh5.jpg" alt="fresh flowers" class="radius">
                      <h5>Fresh 01</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/fresh6.jpg" alt="fresh flowers" class="radius">
                      <h5>Fresh 01</h5>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
              <div id="artificial">
                <h4>Artificial</h4>
                <p>Donec ullamcorper nulla non metus auctor fringilla. Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
                <div class="gallery-wrapper">
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/artificial1.jpg" alt="fresh flowers" class="radius">
                      <h5>Artificial 01</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/artificial2.jpg" alt="fresh flowers" class="radius">
                      <h5>Artificial 02</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/artificial3.jpg" alt="fresh flowers" class="radius">
                      <h5>Artificial 03</h5>
                    </div>
                  </div>
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/artificial4.jpg" alt="fresh flowers" class="radius">
                      <h5>Artificial 04</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/artificial5.jpg" alt="fresh flowers" class="radius">
                      <h5>Artificial 05</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/artificial6.jpg" alt="fresh flowers" class="radius">
                      <h5>Artificial 06</h5>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
              <div id="stick">
                <h4>Stick Werk</h4>
                <p>Donec ullamcorper nulla non metus auctor fringilla. Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
                <div class="gallery-wrapper">
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/stick1.jpg" alt="stick Werk" class="radius">
                      <h5>Stick Werk 01</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/stick2.jpg" alt="stick Werk" class="radius">
                      <h5>Stick Werk 02</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/stick3.jpg" alt="stick Werk" class="radius">
                      <h5>Stick Werk 03</h5>
                    </div>
                  </div>
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/stick4.jpg" alt="stick Werk" class="radius">
                      <h5>Stick Werk 04</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/stick5.jpg" alt="stick Werk" class="radius">
                      <h5>Stick Werk 05</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/stick6.jpg" alt="stick Werk" class="radius">
                      <h5>Stick Werk 06</h5>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
              <div id="souvenir">
                <h4 id="a">Souvenir</h4>
                <p>Donec ullamcorper nulla non metus auctor fringilla. Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
                <div class="gallery-wrapper">
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/so1.jpg" alt="Souvenir" class="radius">
                      <h5>Souvenir 01</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/so2.jpg" alt="Souvenir" class="radius">
                      <h5>Souvenir 02</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/so3.jpg" alt="Souvenir" class="radius">
                      <h5>Souvenir 03</h5>
                    </div>
                  </div>
                  <div class="grid_3 alpha">
                    <div class="pict-wrapp">
                      <img src="images/so4.jpg" alt="Souvenir" class="radius">
                      <h5>Souvenir 04</h5>
                    </div>
                  </div>
                  <div class="grid_3">
                    <div class="pict-wrapp">
                      <img src="images/so5.jpg" alt="Souvenir" class="radius">
                      <h5>Souvenir 05</h5>
                    </div>
                  </div>
                  <div class="grid_3 omega">
                    <div class="pict-wrapp">
                      <img src="images/so6.jpg" alt="Souvenir" class="radius">
                      <h5>Souvenir 06</h5>
                    </div>
                  </div>
                  <div class="clear"></div>
                </div>
              </div>
              
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <?php include"footer.php"; ?>
      <script type="text/javascript">
        // var freshoff=$('#fresh').offset().top;
        // var artificialoff=$('#artificial').offset().top;
        // var stickoff=$('#stick').offset().top;
        // var souveniroff=$('#souvenir').offset().top;
        // // var a=$('#a').offset().top;
        // console.log(freshoff);
        // console.log(artificialoff);
        // console.log(stickoff);
        // console.log(souveniroff);
        // // console.log(a);
        $(window).bind('scroll',function(e){
          var scrolled = $(window).scrollTop();
          console.log(scrolled);
        });

        $(document).ready(function(){
          jQuery('.menu').stickyfloat( {duration: 400} );

          $('#gofresh').click(function(){
            $('html, body').animate({scrollTop:0}, 'slow');
            return false;
          });
          $("#goart").click(function(){
            $('html, body').animate({scrollTop:872}, 'slow');
            return false;
          });
          $("#gostick").click(function(){
            $('html, body').animate({scrollTop:1500}, 'slow');
            return false;
          });
          $("#gosou").click(function(){
            $('html, body').animate({scrollTop:2137}, 'slow');
            return false;
          });

        });
      </script>
    </body>
</html>