<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Produk Kolamku | Bahana Fantastic Pool | Jasa Pembuatan Kolam Renang </title>
      <meta name="description" content="Sebagai salah satu penyedia jasa pembuatan kolam renang, Kolamku.com atau lebih dikenal Bahana fantastic pool juga merupakan perusahaan yang bergerak di bidang water treatment & cleaning chemicals, perencanaan pembuatan dan perawatan kolam renang."/>
      <meta name="keywords" content="kolamku, jasa pembuatan kolam renang, kolam renang"/>
      <meta name="robots" content="index,follow" />
      <meta name="GOOGLEBOT" content="archive" />
      <meta name="author" content="Cloud Paradise"/>
      <link rel="image_src" href="images/paradise.jpg" />
      <link rel="icon" href="images/favicon.png"/>
      <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" type="text/css" href="style/960.css" />
  		<link rel="stylesheet" type="text/css" href="style/style.css" />
      <script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHAd5EtGvzkpyMJ3C_qNEb81ujYCB7rEA&sensor=false"></script>
      <script type="text/javascript" src="js/map.js"></script>
      
      <link rel="stylesheet" href="style/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
      <script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js"></script>
      <script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

      <!-- optional -->
      <link rel="stylesheet" href="style/fancybox/jquery.fancybox-buttons.css" type="text/css" media="screen" />
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-buttons.js"></script>
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-media.js"></script>

      <link rel="stylesheet" href="style/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
      <script type="text/javascript" src="js/fancybox/jquery.fancybox-thumbs.js"></script>     
      <script src="js/css_browser_selector.js" type="text/javascript"></script>
      <script type="text/javascript">
        $(document).ready(function() {
          $(".picture-box").fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            closeBtn    : false,
            helpers   : {
              title : { type : 'inside' },
              buttons : {}
            }
          });
        });
        
      </script>
    </head>
    
    <body>
      <?php include"header.php"; ?>
      <div class="wrapper content-wrapper contact-wrapper">
        <div id="product content" class="container_12 product">
          <div class="grid_12">
            <div class="blue-line"></div>
          </div>
          <div class="clear"></div>
          <div class="grid_12">
            <h2>Product</h2>
          </div>
          <div class="clear"></div>
          <div class="grid_6">
            <p>Pekerjaan kami di tangani oleh tenaga ahli yang berpengalaman di bidangnya dan dengan pertanggungjawaban yang kami utamakan. Selain pembuatan, perawatan dan <span class="italic">service equipment</span> kolam renang Anda yang mengalami <span class="italic">trouble</span> atau masalah kami juga menyediakan filter air yang sesuai dengan kapasitas dan masalah air ditempat Anda, seperti masalah bau, warna, kekeruhan dan bakterologi, kami ada solusinya.</p>
          </div>
          <div class="grid_6">
            <div class="box-thumb">
              <img src="images/thumb6.jpg" alt="kolam renang" >
            </div>
          </div>
          <div class="clear"></div>
          <br>
          <div class="grid_12">
            <h3 id="swimming" class="blue">SWIMING POOL & POOL TOOLS</h3>
          </div>
          <div class="clear"></div>
          <div class="grid_3">
          	<a class="picture-box" href="images/gallery/product1.jpg" rel="fancybox-button">
            <div class="box-thumb">
              <img src="images/thumb7.jpg" alt="product kolam renang" class="radius">
            </div>
            </a>
          </div>
          <div class="grid_3">
          <a class="picture-box" href="images/gallery/product2.jpg" rel="fancybox-button">
            <div class="box-thumb">
              <img src="images/thumb8.jpg" alt="product" class="radius">
            </div>
          </a>
          </div>
          <div class="grid_3">
          <a class="picture-box" href="images/gallery/product3.jpg" rel="fancybox-button">
            <div class="box-thumb">
              <img src="images/product21.jpg" alt="product kolam renang" class="radius">
            </div>
          </a>
          </div>
          <div class="grid_3">
          <a class="picture-box" href="images/gallery/product4.jpg" rel="fancybox-button">
            <div class="box-thumb">
              <img src="images/thumb10.jpg" alt="product kolam renang" class="radius">
            </div>
          </a>
          </div>
          <div class="clear"></div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product17.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product18.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product19.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product20.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="clear"></div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product1.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product2.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product3.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product4.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="clear"></div>
          <br>
          <div class="grid_12">
            <p>Sebagai penyedia jasa pembuatan kolam renang, kami selain merencanakan, design, gambar kerja hingga gambar 3 dimensi pembuatan kolam renang juga menyediakan spare part dan perlengkapan kolam renang dengan harga spesial di antaranya sbb:</p>
            <p class="blue">Pompa & Filter Hayward, Emaux, Astral, Starite, Onga, Grundfose, Wave. Acsesories seperti Inlet fiting,vacuum fiting, Skimmer box, Under Water Light, Maindrain, Mobile vacuum cleaner set, Telescopik handle, wall brush, leaf skimmer, Testkit for cl & ph, Grill lokal/import, pembatas kolam, tangga kolam lokal/import, serta spare part pompa dan filter, salt chlorinator, Pool ionizer, Chloromatic, dll.</p>
            <p>Permainan dan perlengkapan <span class="italic">water park</span> seperti, water slide, ember tumpah, Rizer slide, jamur, aqua rhym, aqua fountain, aqua bow, aqua curtain, aqua console, aqua flow, aqua blest, aquatee, aqua dome, water shooter, aquasphere, aquawave, ban donat, water ball, dll.</p>
            <p>Pekerjaan kami meliputi Semarang, Solo, Jogja, Purwokerto dan Jawa Tengah pada umumnya sampai luar jawa kami juga mengerjakan.</p>
          </div>
          <div class="clear"></div>
          <br>
          <div class="grid_12">
            <h3 id="water" class="blue">WATER TREATMENT</h3>
          </div>
          <div class="clear"></div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product5.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product6.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product7.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product8.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="clear"></div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product9.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product10.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product11.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product12.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="clear"></div>
          <div class="grid_12">
            <p>Bahana water treatment adalah satu divisi dari bahana fantastic pool yang bergerak di bidang perlengkapan water treatment dan purifier  untuk kebutuhan rumah tangga, industri, rumah sakit, hotel, restoran, ruko, sport club, pabrik air minum, dll. Adapun Produk barang dan Jasa diantaranya sbb:</p>
            <p class="blue">Manganese filter, Zeo manganese, carbon filter, water softener filter, Demineralisasi, ultrafiltrasi, Reverse Osmosis system, houshing filter, Filter cartridge, filter frp, carbon block, cartridge quno, cartridge chisso, ultra violet, ozone, procon, FRP wave cyber, media filter: </p>
            <p class="blue">Resin cation-anion, silica sand, carbon aktif, ferrolite, manganese greendsand, greendsand plus, zeolit, lewatit, purolite, ionac, Amberlite, Resin T42na, liquid permanganate, carbon lokal, carbon calgon, Membran RO; Filmtech, membrane CSM, GE/DESAL, LUSO.PH meter, TDS meter, booster pump, element pemanas heater, chemicals untuk pipe cleaning, cleaning deep well, serta produk dari Aplied water treatment, dll.</p>
          </div>
          <div class="clear"></div>
          <br>
          <div class="grid_12">
            <h3 id="chemical" class="blue">CHEMICAL WATER</h3>
          </div>
          <div class="clear"></div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product13.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product14.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product15.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="grid_3">
            <div class="box-thumb">
              <img src="images/product16.jpg" alt="product kolam renang" class="radius">
            </div>
          </div>
          <div class="clear"></div>
          <div class="grid_12">
            <p>Perusahaan Distributor dan supplier bahan kimia yang berkantor di Semarang Jawa Tengah untuk industri water treatment, kolam renang, tambak, dll.</p>
            <p class="blue">Kaporit 60%, kaporit 65%, TCCA 90% Granular, powder & Tablet eX RRT, Chlorin Granular & Tablet Ex shikoku jepang, Sodium hypochloride (kaporit cair)12%, Nissan, Hcl 32%, NAOH, Coustik soda flakes 98%, H2O2, Tawas powder, Tawas Kristal, Diatomide, flocculant, liquid permanganate for oksidator MN%FE, PAC ex Jepang, China & Jerman, Aquades, cupri sulfat/thrusi, Carbon calgon, chlorine & PH test reagen, Algacide, Produk CTX ex ASTRAL, Soda Ash, Garam Australia, anti scallant for membrane RO, pipa dan saluran air, PSG (pembersih serba guna) dan  lainya,</p>
          </div>
          <div class="clear"></div>

        </div>
        
      </div>
      <?php include"footer.php"; ?>
     
    </body>
</html>